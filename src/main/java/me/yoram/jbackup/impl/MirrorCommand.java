package me.yoram.jbackup.impl;

import me.yoram.jbackup.api.ICommand;
import picocli.CommandLine;

public class MirrorCommand implements ICommand {
    @Override
    public String action() {
        return "mirror";
    }

    @Override
    public String description() {
        return "Mirror the client to the server. Example ";
    }
}
