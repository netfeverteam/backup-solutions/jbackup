package me.yoram.jbackup.api;

public interface ICommand {
    String action();
    String description();
}
