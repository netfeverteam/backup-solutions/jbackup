package me.yoram.jbackup;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.security.MessageDigest;

public class JBackup {
    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
    public static void main(String[] args) throws Exception {
        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        File[] files = new File(args[0]).listFiles();

        int nread;
        byte[] buf = new byte[2048];
        for (final File f: files) {
            if (f.isFile()) {
                try (final InputStream in = Files.newInputStream(f.toPath())) {
                    crypt.reset();
                    while ((nread = in.read(buf)) != -1) {
                        crypt.update(buf, 0, nread);
                    }
                    System.out.println(bytesToHex(crypt.digest()) + " - " + f.getAbsolutePath());
                }
            } else {
                main(new String[] {f.getAbsolutePath()});
            }
        }
    }
}
