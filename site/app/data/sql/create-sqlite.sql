create table supervisors(
    id bigint primary key not null,
    user varchar(8) not null unique,
    password varchar(32) not null
);

/* 1-10 */
insert into supervisors(id, user, password) values (1, 'admin', 'a5768359e50990c8ab785af7bcc77802');

create table supervisors_roles(
    id bigint not null primary key,
    supervisors_id int not null,
    role varchar(16) not null,
    UNIQUE(supervisors_id,role)
);

/* 11-20 */
insert into supervisors_roles(id, supervisors_id, role) values (11, 1, 'ADMIN');

create table user_devices(
    id bigint not null primary key,
    connectionType varchar(32) not null,
    deviceId varchar(32) not null,
    deviceBrand varchar(32) not null,
    deviceName varchar(64),
    clientTopic varchar(64) not null,
    lastConnectionStamp INTEGER,
    UNIQUE(connectionType,deviceId)
);

/*
 * Sequence starts at 100
 */
CREATE TABLE SEQUENCE (
    SEQ_NAME VARCHAR(50) NOT NULL primary key,
    SEQ_COUNT DECIMAL(15)
);
INSERT INTO SEQUENCE(SEQ_NAME, SEQ_COUNT) VALUES('SEQ_GEN', 100);

create table user_devices_location(
    id bigint not null primary key,
    user_devices_id bigint not null,
    stamp INTEGER,
    lon real not null,
    lat real not null
);
CREATE INDEX user_devices_location_user_devices_id ON user_devices_location (user_devices_id);
