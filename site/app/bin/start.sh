#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo $DIR
export JBACKUP_HOME="$(dirname "$DIR")"

#export JAVA_HOME=$(which java)
#export JAVA_HOME="/home/yoram/apps/java/j2se/jdk-15.0.1"

$JAVA_HOME/bin/java --enable-preview -Dlogback.configurationFile=${JBACKUP_HOME}/conf/logback.xml -jar $JBACKUP_HOME/lib/jbackup.jar
